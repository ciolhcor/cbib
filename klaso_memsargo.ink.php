<?php
/**
 * @file klaso_memsargo.ink.php
 */

# klaso «regexp» : ^[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*$
# eble kun «\\» por nomspaco

// por klaso
spl_autoload_register(function ($nomo) {
  $dosiernomo = __DIR__.'/'.str_replace('\\', '/', $nomo).'.klaso.php';
  if(is_file($dosiernomo)) require_once $dosiernomo;
}, true, false);


